/* Our own header, to be included *after* all standard system headers */

#ifndef	__OURHDR_H
#define	__OURHDR_H

#include	<sys/types.h>	/* required for some of our prototypes */
#include	<stdio.h>		/* for convenience */
#include	<stdlib.h>		
#include	<sysexits.h>
#include	<string.h>
#include	<unistd.h>	

#define	MAXLINE	4096			/* max line length */
#define MAX_NUM_ARGS        4096    /* Maximum number of arguments to a command */

#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
					/* default file access permissions for new files */
#define	DIR_MODE	(FILE_MODE | S_IXUSR | S_IXGRP | S_IXOTH)
					/* default permissions for new directories */

#define STR_HELPER(x)       #x
#define STR(x)              STR_HELPER(x)
#define ERR_MSG(msg)        (msg __FILE__ STR(__LINE__))

/*!
 * Bool type
 */
typedef enum {
    FALSE = 0,
    TRUE,
} bool;
/* prototypes for our own functions */

void	err_dump(const char *, ...);	/* {App misc_source} */
void	err_msg(const char *, ...);
void	err_quit(const char *, ...);
void	err_ret(const char *, ...);
void	err_sys(const char *, ...);


#endif	/* __OURHDR_H */
