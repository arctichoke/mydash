CC=gcc
CFLAGS=-g  -Wall -I. $(INCLUDES)
MV=/bin/mv

LIBS= -lreadline \
      -lpthread \
      -L. -lmylib

INCLUDES= -I./include \
          -I./lib/include

SRCS=main.c \
     git_version.c \
     ./src/error.c \
     ./src/builtin_cmd.c \
     ./src/arguments.c \
     ./src/job.c

OBJS=$(SRCS:.c=.o)
PROGS=mydash

all: git_version.c $(PROGS)

mydash: $(OBJS)
	@echo [LD] $@
	@$(CC) -o $@ $^ $(LIBS)

.c.o:
	@echo [CC] $@
	@$(CC) $(CFLAGS) -c $< -o $@

dox:
	doxygen doxygen-config

##
## on every build, record the working copy revision string
##
git_version.c: FORCE
		echo -n 'const char* git_version(void) { const char* GIT_Version = "' > git_version.c
		git rev-parse HEAD | cut -c1-4 | tr -d '\n'  >> git_version.c
		echo '"; return GIT_Version; }' >> git_version.c

FORCE:
##
## Then any executable that links in git_version.o will be able
## to call the function git_version() to get a string that
## describes exactly what revision was built.

clean:
	/bin/rm -f *.o $(PROGS) a.out core
	/bin/rm -f *.o $(OBJS) 
